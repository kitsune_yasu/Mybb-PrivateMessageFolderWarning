<?php
/**
 * This file is part of Private Message Folder Warning.
 *
 * Private Message Folder Warning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Private Message Folder Warning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil von Private Message Folder Warning.
 * 
 * Private Message Folder Warning ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 * 
 * Private Message Folder Warning wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

if(!defined("IN_MYBB")) {
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

$plugins->add_hook("global_start", "pmfwarning_notification");

function pmfwarning_info() {
	return array(
		"name"		    => "Private Message Folder Warning",
		"description"   => "Show alert if PM-Box is full.",
		"website"	    => "https://wolfshoehle.eu/mybb/plugins/pmfw",
		"author"	 	=> "Alphawolf_Yasu",
		"authorsite"    => "https://wolfshoehle.eu",
		"version"	    => "2.0",
		"codename"		=> "pmfwarning",
		"compatibility" => "18*"
	);
}

function pmfwarning_install() {
	global $db, $mybb;

	$templates[] = [
		"title" => "pmfwarning_styles",
		"template" => "<style>
		.pmfwarning_alert {
			background: #CCE5FF;
			border: 1px solid #B8DAFF;
			color: #004085;
			border-radius: 6px;
			text-align: center;
			padding: 5px 20px;
			margin-bottom: 15px;
			font-size: 11px;
			word-wrap: break-word;
		}
		.pmfwarning_red {
			background: #F8D7DA;
			border: 1px solid #F5C6CB;
			color: #721C24;
		}
		.pmfwarning_yellow {
			background: #FFF3CD;
			border: 1px solid #FFEEBA;
			color: #856404;
		}
		</style>"
	];

	$templates[] = [
		"title" => "pmfwarning_isfull",
		"template" => "<div class=\"pmfwarning_alert pmfwarning_red\"><strong>{\$lang->pmfwarning_isfull_title}</strong><br />{\$lang->pmfwarning_isfull_text}</div>"
	];

	$templates[] = [
		"title" => "pmfwarning_over_first_percentage",
		"template" => "<div class=\"pmfwarning_alert\"><strong>{\$pmfwarning_warning_title}</strong><br />{\$lang->pmfwarning_warning_text}</div>"
	];

	$templates[] = [
		"title" => "pmfwarning_over_second_percentage",
		"template" => "<div class=\"pmfwarning_alert pmfwarning_yellow\"><strong>{\$pmfwarning_warning_title}</strong><br />{\$lang->pmfwarning_warning_text}</div>"
	];

	foreach ($templates as $template) {
		$insert_array = array(
			"title" => $template["title"],
			"template" => $db->escape_string($template["template"]),
			"sid" => "-1",
			"version" => "",
			"dateline" => time()
		);

		$db->insert_query("templates", $insert_array);
	}

	$setting_group = array(
		"name" => "pmfwarningsettings",
		"title" => "Private Message Folder Warning",
		"description" => "Settings for Private Message Folder Warning",
		"disporder" => 5,
		"isdefault" => 0
	);
	
	$gid = $db->insert_query("settinggroups", $setting_group);

	$setting_array = array(
		"pmfwarning_first_percentage" => array(
			"title" => "Blue Information Box",
			"description" => "Select the minimal percentage to show the blue Box (Default: 75%)",
			"optionscode" => "numeric",
			"value" => 75,
			"disporder" => 1
		),
		"pmfwarning_second_percentage" => array(
			"title" => "Yellow Information Box",
			"description" => "Select the minimal percentage to show the yellow Box (Default: 90%)",
			"optionscode" => "numeric",
			"value" => 90,
			"disporder" => 2
		),
		"pmfwarning_count_mode" => array(
			"title" => "Count-Mode",
			"description" => "Do you want to display the minimum limit to trigger a box or the number of messages the user has?",
			"optionscode" => "select\n0=Show trigger\n1=Show number of messages",
			"value" => 1,
			"disporder" => 3
		)
	);
	
	foreach($setting_array as $name => $setting)
	{
		$setting["name"] = $name;
		$setting["gid"] = $gid;
	
		$db->insert_query("settings", $setting);
	}
	
	rebuild_settings();
}

function pmfwarning_uninstall() {
	global $db;

	$db->delete_query("templates", "title = 'pmfwarning_styles'");
	$db->delete_query("templates", "title = 'pmfwarning_isfull'");
	$db->delete_query("templates", "title = 'pmfwarning_over_first_percentage'");
	$db->delete_query("templates", "title = 'pmfwarning_over_second_percentage'");

	$db->delete_query("settings", "name IN ('pmfwarning_first_percentage','pmfwarning_second_percentage','pmfwarning_count_mode')");
	$db->delete_query("settinggroups", "name = 'pmfwarningsettings'");

	rebuild_settings();
}

function pmfwarning_is_installed() {
	global $db;

	$query = $db->simple_select("templates", "*", "title = 'pmfwarning_isfull'");
	if ($db->num_rows($query) > 0) {
		return true;
	} else {
		return false;
	}
}

function pmfwarning_activate() {
	require_once(MYBB_ROOT . "/inc/adminfunctions_templates.php");
	find_replace_templatesets("header", "#" . preg_quote("{\$pm_notice}") . "#i", "{\$pm_notice}{\$pmfwarning}");
}

function pmfwarning_deactivate() {
	require_once(MYBB_ROOT . "/inc/adminfunctions_templates.php");
	find_replace_templatesets("header", "#" . preg_quote("{\$pmfwarning}") . "#i", "");
}

function pmfwarning_notification(){
	global $mybb, $pmfwarning, $templates, $lang, $pmfwarning_warning_title;

	$lang->load("pmfwarning");

	if ($mybb->usergroup["pmquota"] > 0) {
		if($mybb->user["totalpms"] >= $mybb->usergroup["pmquota"]){
			eval("\$pmfwarning = \"" . $templates->get("pmfwarning_styles") . "\"\n;");
			eval("\$pmfwarning .= \"" . $templates->get("pmfwarning_isfull") . "\";");
		} elseif ($mybb->user["totalpms"] >= (($mybb->settings["pmfwarning_first_percentage"] / 100) * $mybb->usergroup["pmquota"])) {
			if ($mybb->user["totalpms"] >= (($mybb->settings["pmfwarning_second_percentage"] / 100) * $mybb->usergroup["pmquota"])) {
				$pmfwarning_warning_title = pmfwarning_title($mybb->settings["pmfwarning_second_percentage"]);
				eval("\$pmfwarning = \"" . $templates->get("pmfwarning_styles") . "\"\n;");
				eval("\$pmfwarning .= \"" . $templates->get("pmfwarning_over_second_percentage") . "\";");
			} else {
				$pmfwarning_warning_title = pmfwarning_title($mybb->settings["pmfwarning_first_percentage"]);
				eval("\$pmfwarning = \"" . $templates->get("pmfwarning_styles") . "\"\n;");
				eval("\$pmfwarning .= \"" . $templates->get("pmfwarning_over_first_percentage") . "\";");
			}
		}
	}
}

function pmfwarning_title($trigger) {
	global $mybb, $lang;

	if ($mybb->settings["pmfwarning_count_mode"] == 1) {
		return sprintf($lang->pmfwarning_warning_title_actualmode, $mybb->user["totalpms"], $mybb->usergroup["pmquota"]);
	} else {
		return sprintf($lang->pmfwarning_warning_title_triggermode, ceil(($trigger / 100) * $mybb->usergroup["pmquota"]), $mybb->usergroup["pmquota"]);
	}
}