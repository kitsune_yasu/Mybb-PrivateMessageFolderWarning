<?php
/**
 * Language-File for Private Message Folder Warning
 *
 * This file is part of Private Message Folder Warning.
 *
 * Private Message Folder Warning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Private Message Folder Warning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil von Private Message Folder Warning.
 * 
 * Private Message Folder Warning ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 * 
 * Private Message Folder Warning wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

$l['pmfwarning_isfull_title'] = 'Messagelimit reached';
$l['pmfwarning_isfull_text'] = 'You have reached the maximum number of messages and won\'t be able to receive more messages.<br />If you want to continue receiving messages you have to go to your private messages and delete some old messages.<br />Be sure to check the Trash.';

$l['pmfwarning_warning_title_actualmode'] = '%1$d of %2$d messages maximal';
$l['pmfwarning_warning_title_triggermode'] = '%1$d or more of %2$d messages maximal';
$l['pmfwarning_warning_text'] = 'If you reach the maximum number of messages you won\'t be able to receive more messages.<br />To prevent this, you can go to your private messages and delete some old messages.<br />Be sure to check the Trash.';