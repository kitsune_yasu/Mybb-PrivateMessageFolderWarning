<?php
/**
 * Language-File for Private Message Folder Warning
 *
 * This file is part of Private Message Folder Warning.
 *
 * Private Message Folder Warning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Private Message Folder Warning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil von Private Message Folder Warning.
 * 
 * Private Message Folder Warning ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 * 
 * Private Message Folder Warning wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

$l['pmfwarning_isfull_title'] = 'Nachrichtenlimit erreicht';
$l['pmfwarning_isfull_text'] = 'Sie haben die maximale Anzahl an Nachrichten erreicht und werden nicht in der Lage sein weitere Nachrichten zu empfangen.<br />Wenn Sie weiterhin Nachrichten empfangen möchten, müssen Sie in Ihre Privaten Nachrichten gehen und einige ältere Nachrichten löschen.<br />Bitte prüfen Sie auch den Papierkorb!';

$l['pmfwarning_warning_title_actualmode'] = '%1$d von maximal %2$d Nachrichten';
$l['pmfwarning_warning_title_triggermode'] = '%1$d oder mehr von maximal %2$d Nachrichten';
$l['pmfwarning_warning_text'] = 'Wenn Sie die maximale Anzahl an Nachrichten erreichen, werden Sie nicht mehr in der Lage sein weitere Nachrichten zu empfangen.<br />Um dies zu verhindern können Sie in Ihre Privaten Nachrichten gehen und einige alte Nachrichten löschen.<br />Bitte prüfen Sie auch den Papierkorb!';